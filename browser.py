import random
import time

from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

import content
from entities import Task

POSTER_URL = 'https://www.facebook.com/'


class Facebook:
    def __init__(self, task: Task):
        self.task = task
        chrome_options = Options()
        chrome_options.add_argument("--disable-notifications")

        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
        self.driver.maximize_window()

    def close(self):
        try:
            self.driver.quit()
        except:
            pass

    def login(self) -> bool:
        self.driver.get(POSTER_URL)
        time.sleep(3)

        try:
            self.driver.find_element(By.XPATH, "//button[@title='Разрешить все cookie']").click()
        except Exception as e:
            print(e)
        print(time.sleep(1))
        try:
            self.driver.find_element(By.ID, 'email').send_keys(self.task.account.email)
            self.driver.find_element(By.ID, 'pass').send_keys(self.task.account.password)
            self.driver.find_element(By.ID, 'loginbutton').click()
        except Exception as e:
            return self.login()
        time.sleep(5)
        return 'We suspended your account' not in self.driver.page_source and 'Неверные данные' not in self.driver.page_source and 'Неправильный пароль' not in self.driver.page_source and 'Check your email' not in self.driver.page_source

    def scroll(self):
        page_height = self.driver.execute_script("return document.body.scrollHeight")
        scroll_amount = 0

        for _ in range(random.randint(3, 10)):
            scroll_new = random.randint(page_height // 4, page_height // 2)
            for _ in range(10):
                scroll_amount += scroll_new / 10
                self.driver.execute_script("window.scrollTo(0, {})".format(scroll_amount))
                time.sleep(0.1)
            time.sleep(random.randint(3, 6))

    def open_group(self):
        self.driver.get(self.task.link)
        time.sleep(3)
        return 'Этот контент сейчас недоступен' not in self.driver.page_source

    def join_group(self):
        time.sleep(3)
        span_element = self.driver.find_element(By.XPATH, "//span[text()='Присоединиться к группе']")
        if span_element is not None:
            span_element.click()
        time.sleep(3)

    def check_subscription(self):
        try:
            span_element = self.driver.find_element(By.XPATH, "//span[text()='В группе']")
        except:
            return False
        return span_element is not None

    def check_request(self):
        try:
            span_element = self.driver.find_element(By.XPATH, "//span[text()='Отменить запрос']")
        except:
            return False
        return span_element is not None

    def create_post(self):
        self.driver.execute_script("window.scrollTo(0, 0)")
        span = self.driver.find_element(By.XPATH, "//span[text()='Напишите что-нибудь...']")
        span.click()
        time.sleep(8)
        try:
            span = self.driver.find_element(By.XPATH, "//div[@aria-label='Напишите что-нибудь...']")
        except:
            span = self.driver.find_element(By.XPATH, "//div[@aria-label='Создайте общедоступную публикацию…']")
        time.sleep(5)
        span.send_keys(content.text)
        time.sleep(1)
        content.copy_banner()
        try:
            span.send_keys(Keys.COMMAND, 'v')
        except:
            pass
        span.send_keys(Keys.CONTROL, 'v')
        time.sleep(3)
        self.driver.find_element(By.XPATH, "//div[@aria-label='Отправить']").click()
        time.sleep(3)
