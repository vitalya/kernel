import random
import shutil

import requests
from pyjpgclipboard import clipboard_load_jpg

text = """Universal
. Invest in peace of mind with our high-performance doors and windows, engineered
for superior security and energy efficiency. Experience the difference of quality
craftsmanship and professional installation.
. Illuminate your space with our premium windows and doors, designed to maximize
natural light and ventilation while minimizing energy costs. Trust our expert team to
deliver unmatched quality and service.
. Embrace superior comfort and savings with our energy-efficient windows and doors,
crafted with the highest quality materials and technology. Trust our expert team to
deliver unparalleled performance and service.
. Make a grand entrance with our elegant doors, meticulously crafted for timeless
beauty and performance. Experience superior quality and service with our expert
installation.
Illuminate your space with our energy-efficient windows and doors, designed to
enhance your home's comfort and beauty. Trust our expert team to deliver superior
quality and professional installation.
Telephone- 0800 448 0252
Mobile- 07917 483148
Email- info@pcsgaragedoors.co.uk"""

banners = ['https://drive.google.com/file/d/13a5b_1FQukqB6uszIUqjCBkJ6PFO6Ktr/view?usp=sharing',
           'https://drive.google.com/file/d/1G5zFVShUi1AUiiGXOIV5qFfa5Fk-gBbd/view?usp=sharing',
           'https://drive.google.com/file/d/17EqqB0QPuuJu17b86XCkWJ-NDeuccV_I/view?usp=sharing',
           'https://drive.google.com/file/d/1IJgFqdHBjCHDVIPf7Y21QlDEdb1RJrxY/view?usp=sharing',
           'https://drive.google.com/file/d/1DQSk6_C276Xh_9OXuckgMYRXnxhrm1Tq/view?usp=sharing',
           'https://drive.google.com/file/d/1E0KQzNwekT4X7Beq_GiUbkXeszQIbA6r/view?usp=sharing',
           'https://drive.google.com/file/d/1E0KQzNwekT4X7Beq_GiUbkXeszQIbA6r/view?usp=sharing',
           'https://drive.google.com/file/d/1Gd3GJ7kxQYzk8ENuYtqYeWptf43K7fwx/view?usp=sharing',
           'https://drive.google.com/file/d/1kAjoKRdk0sDjtJfyTTOgGh9LYALRrTlQ/view?usp=sharing',
           'https://drive.google.com/file/d/1s7kFxnE3jtRdE4_TZS3yawyjVIm56Q2n/view?usp=sharing',
           'https://drive.google.com/file/d/1J-cpRvY6cuLtatxzcLn26hqVn0k4Gq8q/view?usp=sharing',
           'https://drive.google.com/file/d/1J-cpRvY6cuLtatxzcLn26hqVn0k4Gq8q/view?usp=sharing']


def copy_banner():
    link = random.choice(banners).split('d/')[1].split('/')[0]
    link = f'https://drive.usercontent.google.com/u/0/uc?id={link}&export=download'
    response = requests.get(link, stream=True)
    with open('1.jpg', 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    clipboard_load_jpg("1.jpg")
