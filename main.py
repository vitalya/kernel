import time
from entities import task as task_db
from entities import account as account_db
from browser import Facebook


def main():
    tasks = task_db.get_all_tasks()
    for task in tasks:
        if task.is_posted or not task.is_exists:
            """Если задача опубликована или группа не существует - ничего не делаем"""
            continue
        if task.account is None:
            """Если не назначен исполнитель - назначаем"""
            new_account = account_db.get_available_account()
            if new_account is None:
                """Если нет рабочих аккаунтов"""
                continue
            task_db.connect_account(task, new_account.id)
            continue

        account_db.increment_usage_count(task.account)
        fb = Facebook(task)

        account_is_work = fb.login()
        if not account_is_work:
            """Если аккаунт не рабочий"""
            fb.close()
            account_db.disable_account(task.account)
            task_db.recreate_task(task)
            continue

        # fb.scroll()
        group_is_exist = fb.open_group()
        if not group_is_exist:
            """Если группа не существует"""
            fb.close()
            task_db.mark_group_not_exists(task)
            continue
        # fb.scroll()
        subscription_status = fb.check_subscription()
        if not subscription_status:
            """Если нет подписки"""
            request_status = fb.check_request()
            if not request_status:
                """Если нет заявки"""
                fb.join_group()
                pass
            fb.close()
            continue
        task_db.mark_subscription_status(task, True)
        fb.create_post()
        task_db.mark_posted_status(task, True)
        fb.close()


if __name__ == '__main__':
    while True:
        print(time.time(), 'Started')
        main()
        time.sleep(5)
