FROM python:3.11

WORKDIR /app

# Установка зависимостей
COPY requirements.txt .
RUN pip install -r requirements.txt

# Установка браузера и драйвера (например, Chrome и Chromedriver)
# Примечание: вам может потребоваться адаптировать эти команды в соответствии с вашими потребностями
RUN apt-get update && apt-get install -y \
    wget \
    unzip \
    && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt install -y ./google-chrome-stable_current_amd64.deb \
    && rm ./google-chrome-stable_current_amd64.deb \
    && wget https://chromedriver.storage.googleapis.com/93.0.4577.15/chromedriver_linux64.zip \
    && unzip chromedriver_linux64.zip \
    && mv chromedriver /usr/local/bin/ \
    && rm chromedriver_linux64.zip

# Копирование исходного кода приложения в контейнер
COPY . .

CMD ["python", "main.py"]
