import random

from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from entities.base import Base, engine, session


class Account(Base):
    __tablename__ = 'accounts'
    id = Column(Integer, primary_key=True)
    email = Column(String)
    password = Column(String)
    is_work = Column(Boolean, default=True)
    summary_usage_count = Column(Integer, default=0)

    tasks = relationship("Task", back_populates="account")


Base.metadata.create_all(engine)


def get_available_account():
    accounts = session.query(Account).filter(Account.is_work == True).all()
    if accounts:
        return random.choice(accounts)
    else:
        return None


def increment_usage_count(account):
    account.summary_usage_count += 1
    session.commit()


def disable_account(account):
    account.is_work = False
