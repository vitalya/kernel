from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import dotenv
import os

dotenv.load_dotenv()
engine = create_engine(f'postgresql://{os.getenv("POSTGRESQL_USER")}:{os.getenv("POSTGRESQL_PASSWORD")}@'
                       f'{os.getenv("POSTGRESQL_HOST")}:5432/{os.getenv("POSTGRESQL_DBNAME")}')

Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()
