from sqlalchemy import Column, Integer, String, Boolean, BigInteger

from entities.base import Base, engine, session


class Telegram(Base):
    __tablename__ = 'telegram_users'

    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger)
    full_name = Column(String)
    message_alert_status = Column(Boolean, default=True)
    post_alert_status = Column(Boolean, default=True)


Base.metadata.create_all(engine)
