from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from entities.base import Base, engine, session


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True)
    link = Column(String)
    is_exists = Column(Boolean, default=True)
    is_subscriber = Column(Boolean, default=False)
    is_posted = Column(Boolean, default=False)
    account_id = Column(Integer, ForeignKey('accounts.id'))
    account = relationship("Account", back_populates="tasks")


Base.metadata.create_all(engine)


def get_all_tasks():
    all_tasks = session.query(Task).all()
    return all_tasks


def connect_account(task, account_id):
    task.account_id = account_id
    session.commit()


def recreate_task(task):
    task.account_id = None
    task.is_exists = True
    task.is_subscriber = False
    task.is_posted = False
    session.commit()


def mark_group_not_exists(task):
    task.is_exists = False
    session.commit()


def mark_subscription_status(task, status):
    task.is_subscriber = status
    session.commit()


def mark_posted_status(task, status):
    task.is_posted = status
    session.commit()

